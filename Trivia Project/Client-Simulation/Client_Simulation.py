import socket
import json


SERVER_IP = "127.0.0.1"
SERVER_PORT = 1234
connected = False

def main():
    """
    This function manages all of the other functions in the program.
    Parameters:
    None.
    Returns:
    None.
    """
    sock = connect()
    if sock:
        send_hello(sock)
        send_signup_login_reqs(sock)
        sock.close()


def connect():
	"""
	This function defines the connection by openning and defining a socket.
	Paremeters:
	None.
	Returns:
	bool: indicates if the connection was successful.

	"""
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((SERVER_IP, SERVER_PORT))
	except:
		print("Couldn't connect to the server.")
		sock = False
	return sock


def send_hello(sock):
	"""
	This function is recieving and sending welcome message ('Hello').
	Paremeters:
	socket.
	Returns:
	None.
	"""
	try: 
		if sock.recv(1024).decode() == "Hello": 
			sock.sendall("Hello".encode())
	except ConnectionAbortedError:
		print("Connection aborted.")


def send_signup_login_reqs(sock):
    """
    This function is sending a sign up and login requests.
	Paremeters:
	socket.
	Returns:
	None.
    """
    try:
        signupCode = "0"
        signupRequest = json.dumps({'username': 'ofekNoam1', 'password': '0192', 'email': 'ofekNoam1@gmail.com'})
        signupReqLength = len(signupRequest)
        sock.send(signupCode.encode())
        sock.send(struct.pack('<i', signupReqLength))
        sock.send(signupRequest.encode())
        print(sock.recv(1024).decode()) # print server's reply
        loginCode = "1"
        loginRequest = json.dumps({'username': 'ofekNoam1', 'password': '0192'})
        loginReqLength = len(loginRequest)
        sock.send(loginCode.encode())
        sock.send(struct.pack('<i', loginReqLength))
        sock.send(loginRequest.encode())
        print(sock.recv(1024).decode()) # print server's reply





if __name__ == "__main__":
    main()

