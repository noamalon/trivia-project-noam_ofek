#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <map>
#include <iostream>
#include "Helper.h"
#include "IRequestHandler.h"

class Communicator
{
public:
	Communicator();
	~Communicator();
	void startHandleRequests();
private:
	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);
	std::map<SOCKET, IRequestHandler*> _clients;
	SOCKET _serverSocket;
};

