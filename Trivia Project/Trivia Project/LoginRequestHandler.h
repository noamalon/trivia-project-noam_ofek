#pragma once
#include "IRequestHandler.h"


class LoginRequestHandler : public IRequestHandler
{
	virtual bool isRequestRelevant(RequestInfo req);
	virtual RequestResult handleRequest(RequestInfo req);
};