#include <iostream>

#include "Server.h"

/*
This function manages all of the other functions in the program
input: none
output: 0 (returns to the system that the operation was successful)
*/
int main(void)
{
	Server server;
	ifstream f("config.txt");
	string fileContent;
	getline(f, fileContent);
	getline(f, fileContent);
	server.run();
	return 0;
}
