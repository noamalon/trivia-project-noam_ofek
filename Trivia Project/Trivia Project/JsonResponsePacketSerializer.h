#pragma once
#include <nlohmann/json.hpp>
#include <vector>

#include "Responses.h"

typedef std::vector<char> buffer;

class JsonResponsePacketSerializer
{
public:
    static buffer serializeResponse(ErrorResponse);
    static buffer serializeResponse(LoginResponse);
    static buffer serializeResponse(SignupResponse);

private:
    // Disallow creating an instance of this object
    JsonResponsePacketSerializer() {}
};

