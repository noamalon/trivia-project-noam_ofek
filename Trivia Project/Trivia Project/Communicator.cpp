#include "Communicator.h"
#include "LoginRequestHandler.h"


/*
Communicator's instructor which creates and defines the server socket.
Input: None.
Output: None.
*/
Communicator::Communicator()
{
	WSADATA wsaData = { 0 };
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


/*
Communicator's destructor which closes the server socket and freeing space.
Input: None.
Output: None.
*/
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}


/*
This function defines server's port and IP and allows listening to requests.
Input: None.
Output: None.
*/
void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(1234); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	// Start listening for incoming requests of clients
	if (SOCKET_ERROR == listen(_serverSocket, SOMAXCONN))
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	std::cout << "Listening on port " << 1234 << std::endl;
}


/*
This function calls bindAndListen() func to create server's socket and after gets into an endless loop and listens to client connections.
Input: None.
Output: None.
*/
void Communicator::startHandleRequests()
{
	bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;

		// this accepts the client and create a specific socket from server to this client
		SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client accepted. Server and client can speak" << std::endl;

		// the function that handle the conversation with the client
		std::thread t(&Communicator::handleNewClient, this, client_socket);
		t.detach();
		this->_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, new LoginRequestHandler()));
	}
}


/*
This function handles current client's requests.
Input: socket of current client.
Output: None.
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		// Send and receive welcome message ('Hello')
		Helper::sendData(clientSocket, "Hello");
		Helper::getStringPartFromSocket(clientSocket, 5);
	}
	catch (const std::exception& e)
	{
		closesocket(clientSocket);
	}
}
