#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <WinSock2.h>
#include "Communicator.h"

#define CONNECT_CODE 200

using namespace std;

class Server
{
public:
	Server();
	~Server();
	void run();
private:
	Communicator m_communicator;
};

