#pragma once
#include <ctime>
#include <vector>
#include <iostream>


enum Codes {
	SignupRequest = 0,
	LoginRequest = 1
};


typedef struct RequestInfo
{
	int id;
	std::time_t receivalTime;
	std::vector<unsigned char> buffer;
}RequestInfo;


typedef struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler* newHandler;
}RequestResult;


class IRequestHandler
{
	virtual bool isRequestRelevant(RequestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo) = 0;
};
