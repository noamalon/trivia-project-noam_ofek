#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <functional>
#include <string>

using namespace std;

Server::Server()
{
}

Server::~Server()
{
}


/*
Main function which connects between classes server and communicator and creates the server infrastructure.
Input: None.
Output: None.
*/
void Server::run()
{
	std::thread t_connector(&Communicator::startHandleRequests, m_communicator); // Creating connector thread (which connects between Server::run() func to Communicator::startHandleRequests())
	t_connector.detach();
	std::string request = "";
	while ("EXIT" != request)
	{
		cin >> request;
	}
}
