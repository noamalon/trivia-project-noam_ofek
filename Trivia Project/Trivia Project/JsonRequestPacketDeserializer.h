#pragma once
#include <iostream>
#include <vector>
#include <single_include/nlohman/json.hpp>


typedef struct LoginRequest
{
	std::string username;
	std::string password;
}LoginRequest;


typedef struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
}SignupRequest;


class JsonRequestPacketDeserializer
{
public:
	LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	SignupRequest deserializeSignupRequest(std::vector<unsigned char> buffer);
	void deserializerJsonLogin(const nlohmann::json& j, const LoginRequest& request);
	void deserializerJsonSignup(const nlohmann::json& j, const SignupRequest& request);
private:

};