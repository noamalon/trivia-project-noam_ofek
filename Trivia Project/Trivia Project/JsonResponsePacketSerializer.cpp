#include "JsonResponsePacketSerializer.h"
using nlohmann::json;

void to_json(json& j, const LoginResponse& r) {
	j = json{{"status", r.status}};
}

void to_json(json& j, const SignupResponse& r) {
	j = json{ {"status", r.status} };
}

void to_json(json& j, const ErrorResponse& r) {
	j = json{ {"message", r.message}};
}


buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse r)
{
	buffer buff;
	buff.push_back(ERR);
	nlohmann::json j = r;

	std::vector<std::uint8_t> v_bson = json::to_bson(j);
	int n = v_bson.size();
	unsigned char bytes[4];

	bytes[0] = (n >> 24) & 0xFF;
	bytes[1] = (n >> 16) & 0xFF;
	bytes[2] = (n >> 8) & 0xFF;
	bytes[3] = n & 0xFF;
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(bytes[i]);
	}
	buff.insert(buff.end(), v_bson.begin(), v_bson.end());
	return buff;
}

buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse r)
{
	buffer buff;
	buff.push_back(LOG);
	nlohmann::json j = r;

	std::vector<std::uint8_t> v_bson = json::to_bson(j);
	int n = v_bson.size();
	unsigned char bytes[4];

	bytes[0] = (n >> 24) & 0xFF;
	bytes[1] = (n >> 16) & 0xFF;
	bytes[2] = (n >> 8) & 0xFF;
	bytes[3] = n & 0xFF;
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(bytes[i]);
	}
	buff.insert(buff.end(), v_bson.begin(), v_bson.end());
	return buff;
	return buff;
}

buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse r)
{
	buffer buff;
	buff.push_back(REG);
	nlohmann::json j = r;
	std::vector<std::uint8_t> v_bson = json::to_bson(j);
	int n = v_bson.size();
	unsigned char bytes[4];

	bytes[0] = (n >> 24) & 0xFF;
	bytes[1] = (n >> 16) & 0xFF;
	bytes[2] = (n >> 8) & 0xFF;
	bytes[3] = n & 0xFF;
	for (int i = 0; i < 4; i++)
	{
		buff.push_back(bytes[i]);
	}
	buff.insert(buff.end(), v_bson.begin(), v_bson.end());
	return buff;
}
