#include "LoginRequestHandler.h"

bool LoginRequestHandler::isRequestRelevant(RequestInfo req)
{
	return (req.buffer[0] == Codes::SignupRequest || req.buffer[0] == Codes::LoginRequest);
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo req)
{
	return RequestResult();
}
