#include "JsonRequestPacketDeserializer.h"


LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
	LoginRequest req;
	deserializerJsonLogin(nlohmann::json::from_bson(buffer), req);
	return req;
}


SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
	SignupRequest req;
	deserializerJsonSignup(nlohmann::json::from_bson(buffer), req);
	return req;
}

void JsonRequestPacketDeserializer::deserializerJsonLogin(const nlohmann::json& j, LoginRequest& request)
{
	j.at("username").get_to(request.username);
	j.at("password").get_to(request.password);
}

void JsonRequestPacketDeserializer::deserializerJsonSignup(const nlohmann::json& j, SignupRequest& request)
{
	j.at("username").get_to(request.username);
	j.at("password").get_to(request.password);
	j.at("email").get_to(request.email);
}
